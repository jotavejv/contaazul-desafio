﻿# Considerações Gerais

1 - Nesse teste não utilizei `module bundlers` como Webpack por exemplo, pois utilizei uma ferramenta de tooling nova que tinha intenção de testar (pingy cli), no entanto essa ferramenta não vem com as config nessárias para o bundle e com isso não tive tempo de organizar o ambiente com o webpack.
Levando isso em consideração informo que tenho plena consciência de que para homologar a produção deveriam ter os polyfils necessários.
Sabendo disso, eis o motivo da estrutura de split de código estar no html.

2 - Melhorias que poderiam ser realizadas com tempo extra

- Cadastro mútiplo de carros
- Edição e exclusão múltipla de carros
- Search paginação
- Tooling (webpack, code split, build, minificação...)
- Pixel Perfect com o PSD original
- Validar e testar todos edge cases das rotas

3 - Considerações pessoais

- Infelizmente, tive que realizar horas extra no trabalho durante a semana e por esse motivo penas realizei o teste no final de semana.
Por isso o item 2 de melhorias que poderiam ser realizadas se tivesse tido tempo hábil.
- Em relação ao código, estão boa parte de minhas skills inseridas na codificação, desde organização quanto a lógica.

 
  