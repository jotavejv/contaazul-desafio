﻿# README

### Como rodar oprojeto

`pingy dev`

Esse comando iá compilar o sass e abrir o navegador automáticamente já com o live reload.

## Estrutura

    |-- images
        |-- ContaAzul.png
    |-- scripts
        |-- main.js
        |-- main.babel.js
        |-- dom.js
        |-- utils.js
        |-- routes.js
        |-- fuse.min.js
        |-- navigo.min.js
        |-- store.min.js
    |-- styles
        |-- main.scss
        |-- componentes
            |-- _skin.scss
            |-- animations.scss
            |-- layout.scss
            |-- modal.scss
    |-- index.html
    |-- CONSIDERACOES_GERAIS.md
    |-- README.md